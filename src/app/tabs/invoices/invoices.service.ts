import { Injectable } from '@angular/core';
import { HttpService } from './../../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  constructor(private http: HttpService) { }

  resolve() {
    return this.getAllInvoiceStatus();
  }

  getAllInvoiceStatus() {
    return this.http.get('/spay/portlets/invoicesbystatus');
  }

  getInvoicesByStatus(invoiceStatus) {
    return this.http.get('/invoice-register/' + invoiceStatus);
  }
}
