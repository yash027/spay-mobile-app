import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.page.html',
  styleUrls: ['./invoices.page.scss'],
})
export class InvoicesPage implements OnInit {

  formData: any = [];

  constructor(private activatedRoute: ActivatedRoute) { }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe( data => {
      data = data.data;
      for (let key in data) {
        if(data[key]) {
          this.formData.push({
            status: key,
            data: data[key]
          })
        }
      }
      console.log(this.formData);
    });
  }

  ngOnInit() {
  }

}
