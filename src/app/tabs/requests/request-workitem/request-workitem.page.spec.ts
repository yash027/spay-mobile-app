import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestWorkitemPage } from './request-workitem.page';

describe('RequestWorkitemPage', () => {
  let component: RequestWorkitemPage;
  let fixture: ComponentFixture<RequestWorkitemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestWorkitemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestWorkitemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
