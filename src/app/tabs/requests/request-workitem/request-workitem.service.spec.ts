import { TestBed } from '@angular/core/testing';

import { RequestWorkitemService } from './request-workitem.service';

describe('RequestWorkitemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestWorkitemService = TestBed.get(RequestWorkitemService);
    expect(service).toBeTruthy();
  });
});
