import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-request-workitem',
  templateUrl: './request-workitem.page.html',
  styleUrls: ['./request-workitem.page.scss'],
})
export class RequestWorkitemPage {

  @ViewChild('slides', { static: true }) slides: IonSlides;

  slidesCount: any;
  slideAverage: number = 0;
  slidePercentage: number;
  isBeginning: any;
  isEnd: any;

  formData: any = {};

  constructor(private activatedRute: ActivatedRoute, 
              public global: GlobalService,
              public router: Router) { }

  ionViewWillEnter() {
    this.activatedRute.data.subscribe( data => {
      this.slides.lockSwipes(true);
      this.checkBeginningAndEnd();
      this.slides.length().then( count => {
        this.slidesCount = count;
        this.calculateSlidePercentage(this.slidesCount);
      });
      this.global.hideBottomTabs();
      this.formData = data.data;
      console.log(this.formData);
    }, error => {
      this.global.displayToastMessage('Some Problem Occurred', 1000, 'danger');
      this.router.navigate(['/login']);
    });
  }

  ionViewWillLeave() {
    this.global.showBottomTabs();
  }

  onBack() {
    this.router.navigate(['/tabs/requests'])
  }

  onNext() {
    this.slides.isEnd().then( isEnd => {
      if (!isEnd) {
        this.slideAverage += this.slidePercentage;
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
        this.checkBeginningAndEnd();
      }
    });
  }

  onPrevious() {
    this.slides.isBeginning().then( isStarting => {
      if (!isStarting) {
        this.slideAverage -= this.slidePercentage;
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
        this.checkBeginningAndEnd();
      }
    });
  }

  calculateSlidePercentage(totalSlides) {
    this.slidePercentage = 100/totalSlides;
    this.slideAverage = this.slidePercentage;
  }

  checkBeginningAndEnd() {
    this.slides.isBeginning().then( isBeginning => this.isBeginning = isBeginning );
    this.slides.isEnd().then( isEnd => this.isEnd = isEnd );
  }

}
