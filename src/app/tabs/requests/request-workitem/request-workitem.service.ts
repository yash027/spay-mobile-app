import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RequestWorkitemService {

  constructor(private http: HttpService) { }

  resolve(activatedRoute: ActivatedRoute) {
    const param: any = activatedRoute.params;
    return this.getRequestWorkitemDetails(param.requestId)
  }

  getRequestWorkitemDetails(requestId) {
    return this.http.get('/forms/' + requestId);
  }
}
