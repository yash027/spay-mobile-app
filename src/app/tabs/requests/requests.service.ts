import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: HttpService) { }

  resolve() {
    return this.getRequests();
  }

  getRequests() {
    return this.http.get('/requests/initiate?page=0&size=5000');
  }
}
