import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';
import { ActivatedRoute } from '@angular/router';
import { RequestsService } from './requests.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage implements OnInit {

  requests: any = [];

  constructor(public global: GlobalService, private activatedRoute: ActivatedRoute, private service: RequestsService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe( data => {
      data = data.data;
      this.requests = data;
    });
  }

  doRefresh(event) {
    this.service.getRequests().subscribe( (response: any) => {
      this.requests = response;
      this.global.displayToastMessage('Refreshed Requests', 1000, 'success');
      event.target.complete();
    }, error => {
      this.global.displayToastMessage('Unable To Refresh New Requests', 1000, 'danger');
      event.target.complete();
    });
  }

}
