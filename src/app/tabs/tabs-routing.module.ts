import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TabsPage } from './tabs.page';
import { RequestsService } from './requests/requests.service';
import { RequestWorkitemService } from './requests/request-workitem/request-workitem.service';
import { AuthGuardService } from '../services/auth-guards/auth-guard.service';
import { InvoicesService } from './invoices/invoices.service';

const routes: Routes = [
    {
        path: '',
        component: TabsPage,
        children: [
            {
                path: 'home',
                children: [
                    {
                        path: '',
                        loadChildren: './home/home.module#HomePageModule',
                        canActivate: [ AuthGuardService ]
                    }
                ]
            },
            {
                path: 'requests',
                children: [
                    {
                        path: '',
                        loadChildren: './requests/requests.module#RequestsPageModule',
                        resolve: { data: RequestsService },
                        canActivate: [ AuthGuardService ]
                    },{
                        path: ':requestId',
                        loadChildren: './requests/request-workitem/request-workitem.module#RequestWorkitemPageModule',
                        resolve: { data: RequestWorkitemService },
                        canActivate: [ AuthGuardService ] 
                    }
                ]
            },
            {
                path: 'invoices',
                children: [
                    {
                        path: '',
                        loadChildren: './invoices/invoices.module#InvoicesPageModule',
                        resolve: { data: InvoicesService },
                        canActivate: [ AuthGuardService ]
                    }
                ]
            },
            {
                path: 'inbox',
                children: [
                    {
                        path: '',
                        loadChildren: './inbox/inbox.module#InboxPageModule',
                        canActivate: [ AuthGuardService ]
                    }
                ]
            },
            {
                path: 'more',
                children: [
                    {
                        path: '',
                        loadChildren: './more/more.module#MorePageModule',
                        canActivate: [ AuthGuardService ]
                    }
                ]
            },
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    }

]

@NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
  })
  export class TabsRoutingModule {}