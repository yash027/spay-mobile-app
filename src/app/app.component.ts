import { Component } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private toastController: ToastController  
  ) {
    this.initializeApp();
    router.events.subscribe((routerEvent: Event) => {
      this.checkRouterEvent(routerEvent);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // this.platform.backButton.subscribe( () => {
      //   this.toastController.create(
      //     {
      //       message: 'Back Button Pressed',
      //       duration: 1000,
      //       color: 'secondary'
      //     }
      //   ).then( toast => toast.present());
      // });
    });
  }

  checkRouterEvent(routerEvent: Event): void {

    if (routerEvent instanceof NavigationStart) {
      // loader.present();
    }

    if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      // loader.dismiss();
    }
  }
}
