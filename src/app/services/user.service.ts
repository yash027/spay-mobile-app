import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage';

@Injectable(
    {
        providedIn: 'root'
    }
)

export class UserService {

    constructor(private storage: Storage) {

    }

    public getUsers(key) {
        return this.storage.get(key)
    }

    public saveUser(key, user) {
        return this.storage.set(key, user);
    }

    public deleteUser(key) {
        return this.storage.remove(key);
    }

}