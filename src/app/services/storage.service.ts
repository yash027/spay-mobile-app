import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const userKey = 'user';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(private storage: Storage){}

    getUser() {
        return this.storage.get(userKey);
    }

    saveUser(user) {
        return this.storage.set(userKey, user);
    }

    deleteUser() {
        return this.storage.clear();
    }

}