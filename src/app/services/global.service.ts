import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})

export class GlobalService {

    colors: any = ['primary', 'success', 'secondary', 'tertiary', 'warning', 'danger', 'dark', 'medium'];
    cardBackgrounds: any = ['#4370BF', '#8543BF', '#BF43A6'];
    icons: any = ['pie', 'open', 'list-box', 'albums', 'browsers', 'business', 'card', 'filing'];

    constructor(private toastController: ToastController) { }

    displayToastMessage(msg, duration, color) {
        this.toastController.create(
            {
                message: msg,
                duration: duration,
                color: color
            }
        ).then(toast => toast.present());
    }

    getColorClass(index) {
        if (index >= this.colors.length) {
            while (index >= this.colors.length) {
                index = index - this.colors.length;
            }
        }
        return this.colors[index];
    }

    getIconName(index) {
        if (index >= this.icons.length) {
            while (index >= this.icons.length) {
                index = index - this.icons.length;
            }
        }
        return this.icons[index];
    }

    getCardBackgrounds(index) {
        if (index >= this.cardBackgrounds.length) {
            while (index >= this.cardBackgrounds.length) {
                index = index - this.cardBackgrounds.length;
            }
        }
        return this.cardBackgrounds[index];
    }

    hideBottomTabs() {
        const elem = <HTMLElement>document.querySelector('#tabs');
        if (elem != null) {
            elem.style.display = 'none';
        }
    }

    showBottomTabs() {
        const elem = <HTMLElement>document.querySelector('#tabs');
        if (elem != null) {
            elem.style.display = 'flex';
        }
    }
}