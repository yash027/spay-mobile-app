import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// const baseurl = 'https://spaysaas-development.smartportal-be.smartdocs.ai/payables-server/api';
const baseurl = 'https://spaysaas-dev.smartportal-be.smartdocs.ai/api';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private http: HttpClient){}

    get(url) {
        return this.http.get(baseurl + url);
    }

    post(url, data) {
        return this.http.post(baseurl + url, data);
    }

    put(url, data) {
        return this.http.put(baseurl + url, data);
    }

    delete(url) {
        return this.http.delete(baseurl + url);
    }
    
}