import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public router: Router) {}
  canActivate() {
    // Check whether the loggedInUserName & accessToken are existing or not.
    if (
      !sessionStorage.getItem('Authorization')
    ) {
      // Details are missing. Need navigation to be redirected to Login.
      this.router.navigate(['/login']);
      return false;
    }
    // Details are there. We can allow the navigation to be loaded.
    return true;
  }
}
