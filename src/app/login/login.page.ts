import { Component, OnInit } from '@angular/core';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { LoginService } from './login.service';
import { LoadingController } from '@ionic/angular';
import { GlobalService } from '../services/global.service';
import { StorageService } from '../services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: any = {}

  isFingerPrintAvailable: boolean;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(private faio: FingerprintAIO, 
              private service: LoginService, 
              private loadingController: LoadingController,
              private global: GlobalService,
              private storage: StorageService,
              private router: Router
  ) { }

  ionViewWillEnter() {
    this.showFingerprintAuth();
  }

  ngOnInit() {
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  showFingerprintAuth() {
    this.faio.isAvailable().then(fp => {
      this.isFingerPrintAvailable = true;
    }).catch(error => {
      this.isFingerPrintAvailable = false;
    });
  }

  onLogin(form) {
    this.loadingController.create(
      {
        message: '',
        spinner: 'dots',
      }
    ).then( loader => {
      loader.present();
      this.service.authenticate(this.user).subscribe((sessionToken: any) => {
        sessionStorage.setItem('Authorization', sessionToken.id_token);
        this.service.getAccountDetails().subscribe((user: any) => {
          user['Authorization'] = sessionToken.id_token;
          localStorage.setItem('authorities', user.authorities);
          this.storage.saveUser(user);
          this.global.displayToastMessage('Logged In Successfully', 1000, 'success');
          form.reset();
          this.router.navigate(['tabs']);
          loader.dismiss();
        }, error => {
          this.global.displayToastMessage('Unable to fetch User Details.', 1000, 'danger');
        });
      }, error => {
        if(error.status === 401) {
          this.global.displayToastMessage('Wrong Username and Password', 1000, 'danger');  
        } else {
          this.global.displayToastMessage('Unable to login, Please try again.', 1000, 'danger');
        }
        loader.dismiss();
      });
    })
  }

}
