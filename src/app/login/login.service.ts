import { Injectable } from '@angular/core';
import { HttpService } from '../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpService) { }

  authenticate(user) {
    user['rememberMe'] = true;
    return this.http.post('/authenticate', user);
  }

  getAccountDetails() {
    return this.http.get('/account');
  }

}
